import React, { useState, useCallback, useRef } from 'react';
import { FormGroup, FormControl, Button, Tooltip, OverlayTrigger } from "react-bootstrap";
import appIcon from './app-icon.png';
import { Link } from 'react-router-dom'
import './Login.css';
const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailFocused, setEmailFocused] = useState(false);
  const [passwordFocused, setPasswordFocused] = useState(false);
  const myPopover = useRef(false);

  const validateForm = useCallback( (emailInput) => {
    console.log(emailInput);
    if (emailInput.length === 0 || emailInput.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
      if (myPopover.current){
        myPopover.current.hide();
      }
      return true;
    }
    else {
      if (myPopover.current && emailInput.length !== 0){
        myPopover.current.show();
      }
    }
    
  }, [ ]
  )

  const handleClick = async () => {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        // body: JSON.stringify({ email: email, password: password }), // if POST request should be sent, add this
      });
      if(response.status === 200){
        props.history.push("/welcome");
      }
      else{
        throw new Error("Request failed");
      }
    }
    catch(err) {
      console.log(err);
    }
  };


  return (
    <div className="Login">
      <header className="Login-header">
        <img src={appIcon} className="Login-logo" alt="logo" />
      </header>
      <form onSubmit={handleClick}>
        <FormGroup controlId="email">
          <OverlayTrigger
            key={"top"}
            placement={"top"}
            trigger="manual"
            ref={myPopover}
            overlay={
              <Tooltip id={`tooltip-${'top'}`} style={{ backgroundColor: '#c1272d'}}>
                This is not an email address.
              </Tooltip>
            }
          >
            <FormControl
              className={ (email.length === 0 || validateForm(email)) ? "Input-form" : "Input-error"}
              type="email"
              onFocus={() => setEmailFocused(true)}
              onClick={ () => setEmailFocused(true) }
              onBlur={ () => setEmailFocused(false) }
              placeholder={emailFocused ? "" : "Email"}
              value={email}
              onChange={e => setEmail(e.target.value)}
            />
          </OverlayTrigger>
          
        </FormGroup>
        <FormGroup controlId="password">
          <FormControl
            className="Input-form"
            type="password"
            onFocus={() => setPasswordFocused(true)}
            onClick={ () => setPasswordFocused(true) }
            onBlur={ () => setPasswordFocused(false) }
            placeholder={passwordFocused ? "" : "Password"}
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </FormGroup>

        {/* <OverlayTrigger
          key={'top'}
          placement={'top'}
          overlay={
            <Tooltip id={`tooltip-${'top'}`}>
              Tooltip on <strong>{'top'}</strong>.
            </Tooltip>
          }
        >
          <Button variant="secondary">Tooltip on {'top'}</Button>
        </OverlayTrigger> */}

        <Button className="Button" block onClick={handleClick} disabled={!validateForm(email)}>
          Sign in
        </Button>
        {/* <Overlay show={(email.length !== 0 && !validateForm(email))} placement="top">
          
          <Tooltip id="overlay-example">
            My Tooltip
          </Tooltip>
          
        </Overlay>     */}
      </form>
      <div className="Not-signed">
        <p>
          Not registered yet?
        </p>
        <Link to="/register" className="App-link" style={{ textDecoration: "none" }}>Sign up</Link>
      </div>
    </div>
  );
}

export default Login;