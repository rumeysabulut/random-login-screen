import React from 'react';
import appIcon from './app-icon.png';
import { Link } from 'react-router-dom'
import './App.css';

const selectedStyle = {
  color: 'black',
  textDecoration: 'none',
  fontSize: 50,
  fontFamily: 'Roboto-Thin',
}
  

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={appIcon} className="App-logo" alt="logo" />
      </header>
      <Link to="/login" style={selectedStyle} >START</Link>
    </div>
  );
}

export default App;
